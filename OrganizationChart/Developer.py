from OrganizationChart.Employee import Employee


class Developer(Employee):
    def print(self, spaces):
        prefix = self._build_prefix(spaces)
        print(prefix + self.role + ": " + self.name + " $" + str(self.salary))
