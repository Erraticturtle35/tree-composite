from OrganizationChart.Employee import Employee


class Manager(Employee):
    def print(self, spaces):
        prefix = self._build_prefix(spaces)
        print(prefix + self.role + ": " + self.name + " $" + str(self.salary))
        for employee in self.employees:
            employee.print(spaces + 1)

    def add_employee(self, employee):
        self._employees.append(employee)

    def remove_employee(self):
        self._employees.pop()

    @property
    def employees(self):
        return self._employees

    def __init__(self, manager):
        super(Manager, self).__init__(manager)
        self._employees = []
