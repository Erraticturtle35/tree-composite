from abc import ABC, abstractmethod


class Employee(ABC):
    @property
    def name(self):
        return self._name

    @property
    def salary(self):
        return self._salary

    @property
    def role(self):
        return self._role

    @abstractmethod
    def print(self, spaces):
        pass

    @staticmethod
    def _build_prefix(spaces):
        prefix = ""
        for space in range(spaces):
            prefix += " "
        return prefix

    def __init__(self, employee):
        self._name = employee["name"]
        self._salary = employee["salary"]
        self._role = employee["role"]
