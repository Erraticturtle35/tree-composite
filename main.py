from OrganizationChart.Developer import Developer
from OrganizationChart.Manager import Manager

if __name__ == '__main__':
    ruben = Developer({"name": "Ruben", "salary": 10000, "role": "Developer"})
    martha = Developer({"name": "Martha", "salary": 10000, "role": "Developer"})

    pedro = Manager({"name": "Pedro", "salary": 100000, "role": "Manager"})
    alicia = Manager({"name": "Alicia", "salary": 100000, "role": "Manager"})

    juan = Manager({"name": "Juan", "salary": 1000000, "role": "Manager"})
    pedro.add_employee(ruben)
    pedro.add_employee(martha)
    juan.add_employee(pedro)
    juan.add_employee(alicia)
    juan.print(0)
